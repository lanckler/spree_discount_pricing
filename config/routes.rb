Spree::Core::Engine.routes.append do
  namespace :admin do
    resources :products do
      get :discount_price, :on => :member
      put :put_on_sale, :on => :member
    end
    resources :variants do
      put :put_on_sale, :on => :member
    end
  end
  #namespace :admin do
    #resources :products do
      #post :put_on_sale
    #end
  #end
end
