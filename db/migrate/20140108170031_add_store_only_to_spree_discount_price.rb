class AddStoreOnlyToSpreeDiscountPrice < ActiveRecord::Migration
  def change
    add_column :spree_discount_prices, :store_only, :boolean
  end
end
