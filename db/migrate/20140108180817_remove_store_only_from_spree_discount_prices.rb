class RemoveStoreOnlyFromSpreeDiscountPrices < ActiveRecord::Migration
  def up
    remove_column :spree_discount_prices, :store_only
  end

  def down
    add_column :spree_discount_prices, :store_only, :boolean
  end
end
