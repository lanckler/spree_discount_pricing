class AddInformationToDiscountPricing < ActiveRecord::Migration
  def up
    add_column :spree_discount_prices, :start_at, :datetime
    add_column :spree_discount_prices, :end_at, :datetime
    add_column :spree_discount_prices, :enabled, :boolean
    remove_column :spree_discount_prices, :variant_id
    add_column :spree_discount_prices, :price_id, :integer
  end

  def down
    remove_column :spree_discount_prices, :price_id
    add_column :spree_discount_prices, :variant_id, :integer
    remove_column :spree_discount_prices, :enabled
    remove_column :spree_discount_prices, :end_at
    remove_column :spree_discount_prices, :start_at
  end
end
