SpreeDiscountPricing
====================

Spree Discount Pricing adds a pricing discount strike off to the product


Example
=======

Installation
===========
Add spree_discount_pricing to your Gemfile

    gem 'spree_discount_pricing', :git => 'git@bitbucket.org:zhiyao/spree_discount_pricing'

Run bundler

    bundle install

Install migration & initializer file

    bundle exec rails g spree_discount_pricing:install

Testing
-------

Be sure to bundle your dependencies and then create a dummy test app for the specs to run against.

    $ bundle
    $ bundle exec rake test_app
    $ bundle exec rspec spec

Copyright (c) 2013 [name of extension creator], released under the New BSD License
