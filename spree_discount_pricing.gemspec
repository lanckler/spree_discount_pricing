# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_discount_pricing'
  s.version     = '2.0.0'
  s.summary     = 'Add Spree Discount Pricing Feature to view'
  s.description = 'Add Spree Discount Pricing Feature on the admin interface and show it discount price with the 
                   Original Price as a line-through text decoration'
  s.required_ruby_version = '>= 2.0.0'

  s.author    = 'Kenny Chan'
  s.email     = 'zhiyao.chan@gmail.com'

  #s.files       = `git ls-files`.split("\n")
  #s.test_files  = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core', '~> 2.1.0'

  s.add_development_dependency 'capybara', '1.0.1'
  s.add_development_dependency 'factory_girl', '~> 2.6.4'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'shoulda-matchers'
  s.add_development_dependency 'rspec-rails',  '~> 2.9'
  s.add_development_dependency 'sqlite3'
end
