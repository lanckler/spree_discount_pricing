Deface::Override.new(:virtual_path => "spree/shared/_products",
                     :name => "override_product_display_price",
                     :replace => %q{span.price.selling},
                     :disabled => false) do
                       %q{<%= render :partial => "spree/shared/product_list_display_price", :locals => {:product => product} %>}
                     end

Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                     :name => "override_cart_form_display_price",
                     :replace => %q{span.price.selling},
                     :disabled => false) do
                       %q{<%= render :partial => "spree/shared/product_display_price", :locals => {:product => @product} %>}
                     end

Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                     :name => "override_cart_form_variant_radio_button",
                     :replace => %q{span.variant.option},
                     :disabled => false) do
                       %q{<%= radio_button_tag "products[#{@product.id}]", variant.id, index == 0, 'data-selling' => variant.price_in(current_currency).display_price, 'data-before' => display_original_price(variant), 'data-sku' => variant.sku %>}
                     end