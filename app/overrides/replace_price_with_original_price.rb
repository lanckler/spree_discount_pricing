Deface::Override.new(:virtual_path => "spree/admin/variants/_form",
                     :name => "replace_price_with_original_price",
                     :replace => "[data-hook='price']",
                     :text => %q{
                       <%= f.label :price, Spree.t(:price) %>
                       <%= f.text_field :price, :value => number_to_currency(@variant.original_price, :unit => ''), :class => 'fullwidth' %>
                      },
                     :disabled => false)

Deface::Override.new(:virtual_path => "spree/admin/variants/index",
                     :name => "replace_price_with_original_price",
                     :replace => "[data-hook='variants_row']",
                     :text => %q{
                       <tr>
                       <td class="no-border">
                         <span class="handle"></span>
                       </td>
                       <td><%= variant.options_text %></td>
                       <td class="align-center"><%= number_to_currency(variant.original_price, :unit => '').to_html %></td>
                       <td class="align-center"><%= variant.sku %></td>
                       <td class="actions">
                         <%= link_to_edit(variant, :no_text => true) unless variant.deleted? %>
                         &nbsp;
                         <%= link_to_delete(variant, :no_text => true) unless variant.deleted? %>
                       </td>
                       </tr>
                      },
                     :disabled => false)
