Spree::Admin::VariantsController.class_eval do

  def put_on_sale
    @variant = Spree::Variant.find(params[:id])

    value = params[:variant][:discount_price][:amount]
    enabled = params[:variant][:discount_price][:enabled]
    start_date = params[:variant][:discount_price][:start_date]
    end_date = params[:variant][:discount_price][:end_date]
    @variant.put_on_sale(value, "Spree::Calculator::DollarAmountSalePriceCalculator", true, start_date, end_date, enabled)

    @product = Spree::Product.find(@variant.product_id)
    redirect_to discount_price_admin_product_url(@product)
  end

  private

  # this loads the variant for the master variant discount price editing
  def load_resource_instance
    parent

    if new_actions.include?(params[:action].to_sym)
      build_resource
    elsif params[:id]
      Spree::Variant.find(params[:id])
    end
  end

  def location_after_save
    if @product.master.id == @variant.id and params[:variant].has_key? :discount_price_attributes
      return discount_price_admin_product_variant_url(@product, @variant)
    end

    super
  end

end
