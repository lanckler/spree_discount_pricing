Spree::BaseHelper.class_eval do
  def product_price(product_or_variant, options={})
    options.assert_valid_keys(:format_as_currency, :show_vat_text, :show_calculated_price)
    options.reverse_merge! :format_as_currency => true, :show_vat_text => Spree::Config[:prices_inc_tax]

    amount = options[:show_calculated_price] ? product_or_variant.discount_pricing : product_or_variant.price
    amount += Calculator::Vat.calculate_tax_on(product_or_variant) if Spree::Config[:prices_inc_tax]

    options.delete(:show_calculated_price)
    options.delete(:format_as_currency) ? Spree::Money.new(amount, options) : amount
  end

  def display_original_price(product_or_variant)
    product_or_variant.original_price_in(Spree::Config[:currency]).display_price.to_html
  end

  def display_discount_price(product_or_variant)
    product_or_variant.discount_price_in(Spree::Config[:currency]).display_price.to_html
  end

  def display_discount_percent(product_or_variant, append_text = 'Off')
    discount = product_or_variant.discount_percent_in Spree::Config[:currency]

    if discount > 0
      return "#{number_to_percentage(discount, precision: 0).to_html} #{append_text}"
    else
      return ""
    end
  end

  def get_first_sale!(product_or_variant)
    first_sale = product_or_variant.first_sale
    if first_sale.nil?
      first_sale = Spree::DiscountPrice.new(amount: product_or_variant.original_price.to_f, start_at: Date.today, end_at: Date.today, enabled: false)
    end
    first_sale
  end

end
